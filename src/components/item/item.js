import React, { Component } from "react";
import './item.css';

export class Item extends Component {
    state = {
        isDone: false
    };

    render() {
        console.log(this.props);
        return (<div className="div-title">
            <div className={this.state.isDone ? "div-done" : ""}>{this.props.title}</div>
            <button className="div-item-button" onClick={this.props.onEdit}>✍️</button>
            <button className="div-item-button" onClick={this.props.onDelete}>❌</button>
            <button className="div-item-button" onClick={this.done}>✅</button>
        </div>);
    }

    done = () => {
        this.setState((state) => {
            return {
                isDone: true
            }
        });
    }
}