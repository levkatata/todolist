import React, { Component } from "react";
import { Item } from "../item/item";
import './main.css';

export class Main extends Component {
    state = {
        isShowInput : false,
        todoList : []
    };

    render() {
        return (
            <div className="div-main">
                {!this.state.isShowInput ? <button className="btn-add" onClick={this.showInput}>Add ➕</button> : 
                <div className="div-save">
                    <input type="text"></input>
                    <button onClick={this.saveItem}>Save 📀</button>
                </div>}

                <ul>
                    {this.state.todoList.length !== 0 ? 
                        this.state.todoList.map((item, i) => (
                            <li key={i}><Item title={item} 
                                onDelete={() => this.deleteItem(i)}
                                onEdit={() => this.editItem(i)}
                                /></li>)) 
                        : ""}
                </ul>
            </div>
        );
    } 
    
    showInput = () => {
        this.setState((state) => {
            return {
                ...state,
                isShowInput: true
            }
        });
    }

    saveItem = () => {
        const input = document.querySelector(".div-save > input");
        const newList = [...this.state.todoList, input.value];
        console.log("saveItem for " + newList);

        this.setState((state) => {
            return {
                ...state,
                todoList: newList
            }
        });
    }
    deleteItem(i) {
        const newList = [...this.state.todoList];
        newList.splice(i, 1);
        this.setState((state) => {
            return {
                ...state,
                todoList: newList
            }
        });
        
    }
    editItem(i) {
        console.log("Edit" + i);
        const value = this.state.todoList[i];
        const newValue = prompt("Edit a task", value);
        const newList = [...this.state.todoList];
        newList[i] = newValue;
        this.setState((state) => {
            return {
                ...state,
                todoList: newList
            }
        });
    }
}
