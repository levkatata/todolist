import React from 'react';
import ReactDOM from 'react-dom/client';
import { Main } from './components/main/main';
import './index.css';

const root = ReactDOM.createRoot(document.querySelector('main'));
root.render(
  <React.StrictMode>
    <Main/>
  </React.StrictMode>
);
